**REPORT ASSIGNMENT 1**

Abstract: This report shows the algorithm to combine CIFAR-10 with 'Labeled Faces in the Wild' dataset to have CIFAR-11 dataset which includes 67000 images
in 11 different classes. The CNN network classifying an image across 11 classes includes 6 layers and the Adam optimization algorithm is used. The accuracy of
the model after 200 epochs is 71.18%. Google colab is used to execute Python code.


1) From ['Labeled Faces in the Wild'](http://vis-www.cs.umass.edu/lfw/) (LFW) dataset, randomly pick up 7000 faces (5000 for traning and 2000 for testing)

Dataset preparation: At first, the extracted LFW dataset was uploaded to our Google drive account. The directory of the dataset is My drive/Colab Notebook/Week1.
The dataset contains 5749 subfolders with totally 13000 images. However, the need number of images is 7000 and they are required to be randomly chosen, 
therefore, in the second step, we propose an algorithm whose the pseudocode is presented as follows:

    Input: the needed number of images num.
    Output: a matrix containing num images, each row is an image, its size is num x 3072 (32*32*3 = 3072)
    Algorithm:
    Create a list subfolders containing all 5749 subfolders of the LFW dataset.
    Randomly shuffle the list
    count = 0, flag = False, total_lfw_img = []
    FOR each subfolder in subfolders:
        create a list col containing all images in the subfolder
        FOR each image in col:
            resize img to 32x32
            append it in total_lfw_img
            count = count + 1
            IF count == 7000:
                flag = True
                break
            ENDIF
        ENDFOR
        IF flag == True:
            break
        ENDIF
    ENDFOR
    
The idea of this process is to create a list containing all the subfolders in LFW dataset and randomly shuffle it. Then the first 7000 images are chosen. 
The result of the process is a 7000x3072 matrix (32x32x3 = 3072), each row is an image. The resize step is ncessary because of the difference of the size between CIFAR-10
dataset and LFW dataset. The size of an image in CIFAR-10 is 32x32x3 and that in LFW is larger. In addition, because of our low quality hardware, this 
process took a very long time to complete, the result is then saved into a pickle file in order not to be re-calculated in the next time. 

Finally, the first 5000 images are used as the training set and the rest 2000 images are the test set. Their value are correspondingly stored into 2 variables:
 x_lfw_train and x_lfw_test.

2) Combine the faces in 1) with CIFAR-10 to have CIFAR-11 dataset: 67000 images in 11 classes.

The CIFAR-10 dataset is loaded and stored in 4 variables: x_train_cifar10 (50000 images with the size of 32x32x3), y_train_cifar10 (50000 labels of 10 classes), 
x_test_cifar10 (10000 images with the size of 33x32x3) and y_test_cifar10 (10000 labels of 10 classes). To combine 2 datasets, each image of x_train_cifar10 and x_test_cifar10 
is flattened into a 1x3072 vector. Then, each row of x_lfw_train and x_lfw_test are inserted at the end of 2 reshaped matrices x_train_cifar10 and x_test_cifar10, respectively.
In the next step, each row of former and later matrix is reshaped into 32x32x3 matrix which represents an image. The last label '10' is also inserted into y_train_cifar10 and y_test_cifar10.
As a result, the obtained results are 4 matrices: x_train (55000 images 32x32x3), y_train (55000 labels of 11 classes), x_test (12000 images 32x32x3) and y_test (12000 labels of 11 classes).

Finally, the order of rows in 4 above variables are shuffled. However, the order of images in x must be corresponding to that of labels in y. To solve this problem, the index values of x are randomly shuffled,
and then the row of x and y in the index position will be rearranged to match with the shuffled index values.

3) Build a CNN network to classify an image across 11 classes.

At first, the images from both training set and test set are normalized by dividing by 255 and subtracted by the mean value of x_train. The architecture of the proposed CNN model is: conv (32 filters 3x3, activation function: ReLU, the shape of input: 32x32x3) 
-> maxpooling (pool size: 2x2) -> conv (32 filters 3x3, activation function: ReLU) -> maxpooling (pool size: 2x2) -> conv (32 filters 3x3, activation function: ReLU) -> flatten -> full connection (64 nodes, activation function: ReLU) -> full connection (11 nodes, activation function: softmax).

Training and validation loss

![Training and validation loss](Week1/images/lost.PNG "Image Title")

Training and validation accuracy

![Training and validation accuracy](Week1/images/accuracy.PNG "Image Title")

It can be derived that the model is overfitting because there is a significant difference between the training and validation accuracy. It means that our model performs well with the training set but fail to fit the validation set.